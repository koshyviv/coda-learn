const AWS = require("aws-sdk");
AWS.config.update({
    region: 'ap-south-1'
});
const docClient = new AWS.DynamoDB.DocumentClient();
const csv = require('csvtojson')

const TABLE_NAME = process.env.TABLE_NAME

// TODO stream data from S3 to CSV to Dynamo
module.exports.handler = async (event, context, callback) => {
    console.log('processing for', TABLE_NAME)
    console.log("called SFN", { event })
    let { data, filename, username } = event

    let JSONdata = await getDatafromCSV(data)
    console.log('JSONdata',JSONdata)
    let res = await parseJSON(JSONdata,filename,username)
    console.log('res',res)
}

function getDatafromCSV(data) {
    return new Promise((res, rej) => {
        csv().fromString(data)
            .then(jsonData => {
                console.log("Parsed string to csv", jsonData)
                // parseJSON(jsonData, filename, username)
                res(jsonData)
            })
            .catch(err => {
                console.log("error csv", err)
                rej(err)
            })
    })
}

function parseJSON(JSONdata, filename, username) {
    return new Promise((res, rej) => {
        console.log("called parseJSON")
        let all = []
        JSONdata.forEach(obj => {
            let parsed = {
                PutRequest: {
                    Item: {
                        'id': Math.floor(Math.random() * 10000).toString(),
                        'created_at': new Date().getTime(),
                        'filename': filename,
                        'username': username,
                        ...obj
                    }
                }
            }
            // console.log(parsed)
            all.push(parsed)
        });
        console.log("json to write to db", all)

        let obj = {}
        obj[TABLE_NAME] = all
        docClient.batchWrite({
            RequestItems: obj
        }, function (err, data) {
            console.log("afeter ddb", { err, data })
            res({ err, data })
        })
    })

}

