const AWS = require("aws-sdk");
AWS.config.update({
    region: 'ap-south-1'
});
const docClient = new AWS.DynamoDB.DocumentClient();

const TABLE_NAME = process.env.TABLE_NAME
const DELETE_THRESHOLD_DAYS = process.env.DELETE_THRESHOLD_DAYS
module.exports.handler = async (event, context, callback) => {
    console.log('processing for', TABLE_NAME, event)
    await ddbGetUser(TABLE_NAME)
}


// TODO Query is very inefficient - Update DB model design
let ddbGetUser = async function (table) {

    let getParams = { TableName: table };
    let result = await docClient.scan(getParams).promise()
    for (item of result.Items) {
        let deleteParams = { Key: { id: item.id, created_at: item.created_at }, TableName: table }
        let dataAge = (new Date().getTime() - item.created_at) / (1000 * 60 * 60 * 24)
        console.log("each item in scan", { item, deleteParams, dataAge })
        if (dataAge > THRESHOLD_DAYS) {
            console.log("delete", { item, dataAge })
            await docClient.delete(deleteParams).promise()
        }
    }

}


