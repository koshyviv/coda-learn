const AWS = require('aws-sdk');

var AmazonCognitoIdentity = require('amazon-cognito-identity-js');
var CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
var AuthenticationDetails = AmazonCognitoIdentity.AuthenticationDetails;
var CognitoUser = AmazonCognitoIdentity.CognitoUser;

const USERPOOLID = process.env.USERPOOLID
const CLIENTID = process.env.CLIENTID
const REGION = process.env.REGION


var poolData = {
    UserPoolId: USERPOOLID,
    ClientId: CLIENTID
};
var userPool = new CognitoUserPool(poolData);

AWS.config.region = REGION;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: USERPOOLID,
});



module.exports.login = async function (event, context, callback) {
    console.log("request",event.body)

    // consosldsad;
    const requestBody = JSON.parse(event.body);
    console.log("🚀 ~ file: userAuth.js ~ line 27 ~ requestBody", requestBody)
    try {
        let res = await login(requestBody)
        callback(null, {
            statusCode: 201,
            body: JSON.stringify({
                res
            }),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        })
    } catch (error) {
        callback(null, {
            statusCode: 400,
            body: JSON.stringify({
                data: error
            }),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        })
    }
}

// TODO Update confirmation flow
module.exports.register = async function (event, context, callback) {
    const requestBody = JSON.parse(event.body);
    console.log("🚀 ~ file: userAuth.js ~ line 62 ~ register | requestBody", requestBody)
    try {
        let res = await register(requestBody)
        console.log("🚀 ~ file: userAuth.js ~ line 65 ~ register | res", res)
        callback(null, {
            statusCode: 201,
            body: JSON.stringify({
                data: res
            }),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        })
    } catch (error) {
        callback(null, {
            statusCode: 400,
            body: JSON.stringify({
                data: error
            }),
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        })
    }
}


function login(requestBody) {
    var username = requestBody.username;
    var password = requestBody.password;

    var authenticationData = {
        Username: username,
        Password: password
    };
    var authenticationDetails = new AuthenticationDetails(authenticationData);
    var userData = {
        Username: username,
        Pool: userPool
    };
    console.log("🚀 ~ file: userAuth.js ~ line 92 ~ login ~ userData", userData)
    var cognitoUser = new CognitoUser(userData);

    return new Promise((res, rej) => {
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log("Success login", result);
                let accessToken = result.getAccessToken().getJwtToken()
                let idtoken = result.getIdToken().getJwtToken()
                console.log("🚀 ~ file: userAuth.js ~ line 101 ~ returnnewPromise ~ idtoken", idtoken)
                console.log("🚀 ~ file: userAuth.js ~ line 100 ~ returnnewPromise ~ accessToken", accessToken)
                res({accessToken,idtoken})
            },
            onFailure: function (err) {
                console.log("🚀 ~ file: userAuth.js ~ line 104 ~ returnnewPromise ~ err", err)
                console.log("Failed login", err)
                rej(err)
            }
        });
    })
}

function register(requestBody) {
    var attributeList = [];

    var dataEmail = {
        Name: 'email',
        Value: requestBody.email,
    };
    var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
    attributeList.push(attributeEmail);
    // event.body
    return new Promise((res, rej) => {
        userPool.signUp(requestBody.username, requestBody.password, attributeList, null, function (
            err,
            result
        ) {
            if (err) {
                alert(err.message || JSON.stringify(err));
                rej(err);
            }
            var cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsername());
            res(cognitoUser.getUsername())
        });
    })
}
