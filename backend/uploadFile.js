const AWS = require("aws-sdk");
var s3 = new AWS.S3();
var ddb = new AWS.DynamoDB();
const stepfunctions = new AWS.StepFunctions();

const TABLE_NAME = process.env.TABLE_NAME
const BUCKET = process.env.BUCKET
const SF_ARN = process.env.statemachine_arn;

module.exports.handler = async (event, context, callback) => {

    if (!event.requestContext.authorizer) sendResponse(callback, 501, 'Auth provider is missing')
    const username = event.requestContext.authorizer.claims['cognito:username'];
    console.log("user", username)

    let S3response = ''
    let dataAsText =''
    let filename = ''
    console.log("the entire event", event)

    // save to S3
    try {
        let {buffer,text} = convertFileToBuffer(event.body)
        dataAsText = text
        let res = await uploadToS3Data(buffer)
        S3response = res
        filename = res.key
        console.log("Saved to S3", res)
    } catch (error) {
        console.log("Error while saving to S3", error)
        sendResponse(callback, 500, error)
        return;
    }

    //make entry in Dynamo
    try {
        items = {
            'id': { S: Math.floor(Math.random() * 10000).toString() },
            'created_at': { N: new Date().getTime().toString() },
            'file': { S: S3response.Key },
            'username': { S: username }
        }
        let res = await ddbCreate(TABLE_NAME, items)
        console.log("saved to dynamo", res)
        sendResponse(callback, 200, { db: items, s3: S3response })
    } catch (error) {
        console.log("Error while saving to DDb", error)
        sendResponse(callback, 500, error)
    }

    invokeStepFunction(dataAsText, filename, username)
};

let convertFileToBuffer = function(data){
    let buffer = Buffer.from(data, 'base64');
    let text = buffer.toString('utf-8')
    return {buffer,text}
}

let uploadToS3Data = function (data) {
    console.log("data is read", data)
    var bucketData = {
        Bucket: BUCKET,
        Key: Date.now() + ".csv",
        Body: data,
        ContentType: 'text/csv',
        ACL: 'public-read'
    };
    console.log('saving to S3', bucketData)
    return s3.upload(bucketData).promise()
}

// TODO move to utils
let ddbCreate = function (table, item) {
    var params = {
        TableName: table,
        Item: item
    };
    console.log('saving to dynamo', params)
    return ddb.putItem(params).promise()
}

// TODO move to utils
let sendResponse = function (callback, statusCode, data) {
    callback(null, {
        statusCode: statusCode,
        body: JSON.stringify(data),
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    })
}


let invokeStepFunction = function (dataAsText, filename ,username) {
    console.log("Started step function exectuion",SF_ARN)
    console.log("recieved data",dataAsText)
    const params = {
        stateMachineArn : SF_ARN,
        input: JSON.stringify({
            data: dataAsText,
            filename,
            username
        })
    }
    return stepfunctions.startExecution(params).promise().then(() => {
        console.log(`Your statemachine ${SF_ARN} executed successfully`);
    }).catch(error => {
        console.log(error.message);
    });
}