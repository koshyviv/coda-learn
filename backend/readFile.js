const AWS = require("aws-sdk");
var s3 = new AWS.S3();
var ddb = new AWS.DynamoDB();
const csv=require('csvtojson')
const fs = require('fs')

const TABLE_NAME = process.env.TABLE_NAME
const DATA_TABLE_NAME = process.env.DATA_TABLE_NAME
const BUCKET = process.env.BUCKET

module.exports.handler = async (event, context, callback) => {

    if (!event.requestContext.authorizer) sendResponse(callback, 501, 'Auth provider is missing')
    const username = event.requestContext.authorizer.claims['cognito:username'];
    console.log("user", username)

    let filesdata = await ddbGetUser(TABLE_NAME, username)
    let parsedData = await ddbGetUser(DATA_TABLE_NAME, username)
    sendResponse(callback, 200, {filesdata,parsedData})
}


// TODO Replace Scan with sort on primary key
let ddbGetUser = function (table, username) {
    var params = {
        TableName: table,
        FilterExpression: 'username = :username',
        ExpressionAttributeValues: {
            ":username": { S: username }
        }
    };
    return new Promise((res, rej) => {
        ddb.scan(params, function (err, data) {
            if (err) {
                rej(err)
            } else {
                res(data.Items)
            }
        });
    })
}


let sendResponse = function (callback, statusCode, data) {
    callback(null, {
        statusCode: statusCode,
        body: JSON.stringify(data),
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    })
}


